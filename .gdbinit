# Board struct 24 bytes (on 64 bit hw)
#
# int rows          (4 bytes)
# int cols          (4 bytes)
# int mines         (4 bytes)
# <padding>         (4 bytes)
# square_t ** board (8 bytes)

# Handling boundary conditions is done by inserting a hidden bogus-boundary
# around the board: rows + 2 and columns + 2

# Pretty print the uncovered board from memory
define pretty_print_game 
  if $argc == 1
    set var $addr = $arg0

    set var $rows = $addr->rows + 2
    set var $cols = $addr->columns + 2

    printf "Board is %d × %d\n", $rows, $cols

    set var $x = 1

    while $x < ($rows - 1)

      set var $y = 1
      while $y < ($cols - 1)

        set var $square = $addr->board[$x][$y]
        
        if $square.mine == 1
          printf "m "
        else
          if $square.around == 0
            printf "  "
          else
            printf "%d ", $square.around
          end
        end

        set var $y = $y + 1
      end

      printf "\n"

      set var $x = $x + 1
    end
  end
end

# Clear clear the board by `clicking' all the safe squares
define safe_squares
  if $argc == 1
    set var $addr = $arg0

    set var $rows = $addr->rows + 2
    set var $cols = $addr->columns + 2

    set var $x = 1
    while $x < ($rows - 1)

      set var $y = 1
      while $y < ($cols - 1)

        set var $square = $addr->board[$x][$y]
        
        if $square.mine != 1
          printf "playing %d,%d\n", $x, $y
          # Update the square in the game window
          #call xdemineur_square_play($x, $y)
          # Play the square
          call demineur_play($x, $y)
        end

        set var $y = $y + 1
      end

      printf "\n"

      set var $x = $x + 1

    end
  end
end

# flag all mines
define mines
  if $argc == 1
    set var $addr = $arg0

    set var $rows = $addr->rows + 2
    set var $cols = $addr->columns + 2

    set var $x = 1
    while $x < ($rows - 1)

      set var $y = 1
      while $y < ($cols - 1)

        set var $square = $addr->board[$x][$y]
        
        if $square.mine == 1
          printf "flagging %d,%d\n", $x, $y

          # Put the flag on the square
          call demineur_flag_question($x, $y)

          # Update the mine counter in the game window
          call xdemineur_mines($x, $y)

          #  Update the square in the game window. `Puts the graphical flag on'
          call xdemineur_square($x, $y)
        end

        set var $y = $y + 1
      end

      printf "\n"

      set var $x = $x + 1

    end
  end
end

# Get information about square pointers. For debugging fetch_remote_square
define square_ptr
  if $argc == 3
    set var $board  = $arg0
    set var $x      = $arg1
    set var $y      = $arg2

    set var $square = &$board->board[$y][$x]

    printf "Board base      is at %p\n", $board
    printf "Field base      is at %p\n", $board->board
    printf "Row %02d          is at %p\n", $y, $board->board[$y]
    printf "Col %02d (square) is at %p\n", $x, $square

    printf "\n"
    printf "Board details:\n"
    x/24xb $board
    p/x *$board
    p   *$board

    printf "\n"
    printf "Square details\n"
    x/12b $square
    p/x *$square
    p   *$square
  end
end

# Get row information. Print pointers to individual cells for a given row
define get_row
  if $argc == 2
    set var $board  = $arg0
    set var $x      = $arg1

    set var $y = 1
    while $y < ($board->columns + 1)
      set var $square = $board->board[$x][$y]
      printf "square(%d,%d) %p = { mine = %d, around = %d }\n", $x, $y, &($board->board[$x][$y]), $square.mine, $square.around
      set var $y = $y + 1
    end
  end
end
