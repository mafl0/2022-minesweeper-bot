
BUILDDIR=build
SRCDIR=src

CC=clang

CFLAGS+=-g
CFLAGS+=-O0
CFLAGS+=-Wall
CFLAGS+=-Wpedantic
CFLAGS+=-I$(SRCDIR)

CFILES+=$(SRCDIR)/main.c
CFILES+=$(SRCDIR)/debug.c
CFILES+=$(SRCDIR)/ptrace.c

default: $(BUILDDIR)/main

$(BUILDDIR):
	mkdir -p $@

$(BUILDDIR)/main: $(CFILES) | $(BUILDDIR)
	$(CC) $(CFLAGS) -o $@ $^

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)

.PHONY: tags
tags:
	ctags -R -o tags $(SRCDIR)
