#!/usr/bin/env bash

make -B

# Test if xdemineur is running, start it if it's not. Get the pid
pgrep xdemineur &> /dev/null
if [[ $? == 0 ]]; then
  XDMPID=$(pgrep xdemineur)
  echo "already running at $XDMPID"
else
  $PWD/xdemineur/xdemineur &
  XDMPID=$!
  echo "was not running. Now it is at $XDMPID"
fi

echo "Base addr according to mapfile: $(cat "/proc/$XDMPID/maps" | head -n 1 | cut -f1 -d'-')"

if [[ $1 == "d" ]]; then
  echo "sudo gdb -p $XDMPID"
  sudo gdb -p $XDMPID
else
  echo "sudo ./build/main -p $XDMPID"
  sudo ./build/main -p $XDMPID
fi
