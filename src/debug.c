#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <unistd.h>

#include "debug.h"

static char _line[DEBUG_BUFFER_SIZE];

void _reset() {
  for (size_t i = 0; i < DEBUG_BUFFER_SIZE; ++i)
    _line[i] = '\0';
}

void Log(const char * fmt, ...) {
  va_list args;
  va_start(args, fmt);
  vsnprintf(_line, DEBUG_BUFFER_SIZE, fmt, args);
  fwrite(_line, DEBUG_BUFFER_SIZE, sizeof(char), stderr);
  va_end(args);
  _reset();
}

void hexdump(uint8_t * src, size_t len) {
  for (size_t i = 0; i < len; ++i) {
    if (i%8==0) {
      Log("\n%16p: ", (void *) (src + i));
    }
    Log("%02x ", src[i]);
  }
  Log("\n");
}
