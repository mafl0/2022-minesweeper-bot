#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stdint.h>

#define DEBUG_BUFFER_SIZE 256

void Log(const char * fmt, ...);
void hexdump(uint8_t * src, size_t len);

#endif // __DEBUG_H__
