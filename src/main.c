#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <sys/user.h>

#include "xdemineur.h"
#include "debug.h"
#include "ptrace.h"

// To circumvent the compiler's opinion about the alignment of my pointers
#define PTRADD(x, y) (void *) ((long) (x) + (long) (y))

/**
 * Copy the board data structure
 */
board_t * fetch_board_struct(pid_t p, void * addr) {
  board_t * b = (board_t *) calloc(1, sizeof(board_t));
  return peeks(b, p, addr, sizeof(*b));
}

square_t * fetch_remote_square(pid_t p, square_t * s, board_t * remote_board, size_t x, size_t y) {
  // Get the pointer to the row from the base pointer of the board
  void * ptr = PTRADD( remote_board->board, y * sizeof(s));
  ptr  = (void *) peek(p, ptr);  

  // Get the pointer to the square from the base pointer of the row
  ptr = PTRADD(ptr, x * sizeof(*s));
  peeks(s, p, ptr, sizeof(*s));

  return s;
}

/**
 * Get a copy of the remote board
 */

int8_t * fetch_board(pid_t p, board_t * b) {

  // Return buffer and iteration temporary variable
  int8_t * ret = (int8_t *) calloc((b->columns + 2) * (b->rows + 2), sizeof(int8_t));
  int8_t * iter = ret;

  square_t * s = (square_t *) calloc(1, sizeof(square_t));

  for (size_t y = 0; y < b->rows + 2; ++y) {
    for (size_t x = 0; x < b->columns + 2; ++x) {
      fetch_remote_square(p, s, b, x, y);

      if (s->mine) *iter = -1;
      else *iter = (int8_t) (s->around & 0xFF);

      iter += sizeof(*iter);
    }
  }

  // Clean up after ourselves...
  free(s);

  return ret;
}

int main(int argc, char ** argv) {

  char option;
  pid_t p = -1;

  // Parse command line options, p required
  while ((option = getopt(argc, argv, "p:")) != -1) {
    switch(option) {
      case 'p':
        p = (pid_t) atoi(optarg);
        break;
      default:
        // not supported
        return -1;
        break;
    }
  }

  if (p == -1) {
    Log("No PID provided. PID is required\n");
    abort();
  }

  void * base_addr = (void *) base_address(p); 
  board_t * b;

  attach(p);

  b = fetch_board_struct(p, base_addr + REMOTE_BOARD_OFFS);
  int8_t * result = fetch_board(p, b);

  for (size_t y = 0; y < b->rows+ 2; ++y) {
    for (size_t x = 0; x < b->columns + 2; ++x) {
      if (*result == -1)
        Log("M ");
      else {
        if(*result == 0)
          Log("  ", *result);
        else {
          Log("%d ", *result);
        }
      }
      result += sizeof(*result);
    }
    Log("\n");
  }

  detach(p);

  return EXIT_SUCCESS;
}
