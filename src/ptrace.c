#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <sys/ptrace.h>
#include <sys/wait.h>

#include "debug.h"

void attach(pid_t p) {
  
  if (ptrace(PTRACE_ATTACH, p, 0L, 0L) == -1L) {
    Log("failed to attach to %d, %s\n", p, strerror(errno));
    
    abort();
  }

  /* wait for the SIGSTOP to take place. */
  if (waitpid(p, NULL, 0L) == -1) {
    Log("there was an error waiting for the target to stop: %s\n", strerror(errno));
    
  }
}

void detach(pid_t p) {

  if (ptrace(PTRACE_DETACH, p, 0L, 0L) == -1L) {
    Log("failed to detach from %d, %s\n", p, strerror(errno));
    abort();
  }
}

/**
 * Read the base address from the maps file:
 *
 * 1. Compute the /proc/<pid>/maps path and open it with fopen
 * 2. read and parse character-by-character hexdigits until we encounter '-',
 *    discard the first ' ' character.
 */
long base_address(pid_t p) {
  long ret = 0;
  char buf[256];

  snprintf(buf, 256, "/proc/%u/maps", p);
  FILE * map_file = fopen(buf, "r");

  for (char c = fgetc(map_file); c != '-'; c = fgetc(map_file)) {
    ret <<= 4;                // on to the next nibble
    if ('0' <= c && c <= '9') {
      ret += c - '0';         // decdigit
    } else {
      ret += (c - 'a') + 0xa; // hexdigit, lowercase
    }
  } 

  fclose(map_file);

  return ret;
}

/**
 * Read a /single/ address from remote memory, checking errno as is suggested in
 * `man 3 ptrace'
 */
long peek(pid_t p, void * remote_addr) {
  long ret;

  errno = 0;

  ret = ptrace(PTRACE_PEEKDATA, p, remote_addr, 0L);

  if (errno) Log("%s while accessing ptr %16p\n", strerror(errno), (void *) remote_addr);

  return ret;
}

/**
 * Read a block of memory starting at @remote_base@ and running for @len@ bytes.
 * We are copying a long (usually 8 bytes) at a time.
 */
void * peeks(void * dest, pid_t p, void * remote_base, size_t len) {

  long * buf = (long *) calloc(sizeof(char), len);

  for (size_t i = 0; i * sizeof(*buf) < len; ++i)
    buf[i] = peek(p, remote_base + i * sizeof(*buf));

  memcpy(dest, buf, len);
  free(buf);

  return dest;
}

/**
 * Poke a single word in the remote address space
 */
long  poke(long src, pid_t p, void * remote_addr) {
  long ret;

  errno = 0;

  ret = ptrace(PTRACE_POKEDATA, p, remote_addr, src);

  if (errno) Log("%s while accessing ptr %16p\n", strerror(errno), (void *) remote_addr);

  return ret;
}

/**
 *
 */
void * pokes(long * src, pid_t p, void * remote_base, size_t len) {
  for (size_t i = 0; i < len; ++i)
    poke(src[i], p, remote_base + i * sizeof(*src));
}
