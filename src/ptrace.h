#ifndef __PTRACE_H__
#define __PTRACE_H__

void attach(pid_t p);
void detach(pid_t p);

long base_address(pid_t p);

long   peek(pid_t p, void * remote_addr);
void * peeks(void * dest, pid_t p, void * remote_base, size_t len);

long   poke(long src, pid_t p, void * remote_addr);
void * pokes(long * src, pid_t p, void * remote_base, size_t len);

#endif // __PTRACE_H__
