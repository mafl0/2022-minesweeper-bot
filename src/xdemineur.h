#ifndef __XDEMINEUR_H_
#define __XDEMINEUR_H_

/* board variable in xdemineur static mem */
#define REMOTE_BOARD_OFFS 0x11190

/* xdemineur_play() in xdemineur .text */
#define REMOTE_PLAY_OFFS  0x00107e0c

/** 
 * Taken from the source code of Xdemineur. The structs that contain the
 * information of the `board' and the `square'
 */

typedef struct square {
   int   mine ;     /* 1 if there's a mine, 0 otherwise */
   int   around ;   /* mine count around this square    */
   enum {
      HIDDEN    ,   /* hidden square    */
      FLAGGED   ,   /* flagged square   */
      QUESTION  ,   /* question mark    */
      UNCOVERED     /* uncovered square */
   }
   state ;          /* square state */
}
square_t;

typedef struct board {                                               
   int        rows ;      /* number of rows    */
   int        columns ;   /* number of columns */
   int        mines ;     /* number of mines   */
   square_t   **board ;   /* the game board    */
} board_t;                                     

/**
 * Initialization of the board:
 *
 * board.board = (square_t **) malloc((board.rows + 2) * sizeof(square_t *));
 * for (row = 0; row < board.rows + 2; row++)
 *    board.board[row] = (square_t *) malloc((board.columns + 2) * sizeof(square_t));
 *
 * board.board is a consecutive `column of rows', each column is a consecutive block of squares.
 *
 * -> board.board is an array of pointers to X
 * -> X is an array of pointers to squares
 */

#endif // __XDEMINEUR_H_
